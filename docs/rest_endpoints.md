# huscy.data_protection

## Right of access by the data subject

* `/api/dataaccessrequests/` **[GET]**
    * list data access requests
    * required permissions: `is_superuser`
* `/api/dataaccessrequests/` **[POST]**
    * create new data access request
    * required permissions: `is_authenticated`
* `/api/dataaccessrequests/<dataaccessrequest_id>/` **[DELETE]**
    * delete data access request
    * required permissions: `is_superuser`
* `/api/dataaccessrequests/<dataaccessrequest_id>/apply` **[POST]**
    * process data access request
    * required permissions: `is_superuser`

## Right to erasure

* `/api/datarevocationrequest/` **[GET]**
    * list data revocation requests
    * required permissions: `is_superuser`
* `/api/datarevocationrequest/` **[POST]**
    * create new data revocation request
    * required permissions: `is_authenticated`
* `/api/datarevocationrequest/<datarevocationrequest_id>/` **[DELETE]**
    * delete data revocation request
    * required permissions: `is_superuser`
* `/api/datarevocationrequest/<datarevocationrequest_id>/apply` **[POST]**
    * process data revocation request
    * required permissions: `is_superuser`
