import pytest
from model_bakery import baker

from rest_framework.test import APIClient


@pytest.fixture
def user(django_user_model):
    return django_user_model.objects.create_user(username='user', password='password',
                                                 first_name='Franz', last_name='Hose')


@pytest.fixture
def admin_client(admin_user):
    client = APIClient()
    client.login(username=admin_user.username, password='password')
    return client


@pytest.fixture
def client(user):
    client = APIClient()
    client.login(username=user.username, password='password')
    return client


@pytest.fixture
def anonymous_client():
    return APIClient()


@pytest.fixture
def contact():
    return baker.make('subjects.Contact')


@pytest.fixture
def subject(contact):
    return baker.make('subjects.Subject')


@pytest.fixture
def attribute_schema():
    schema = {
        'type': 'object',
        'properties': {
            'attribute1': {'type': 'integer'},
            'attribute2': {'type': 'string'},
        },
    }
    return baker.make('attributes.AttributeSchema', schema=schema)


@pytest.fixture
def data_access_request(subject):
    return baker.make('data_protection.DataAccessRequest', contact=subject.contact)


@pytest.fixture
def data_revocation_request(subject):
    return baker.make('data_protection.DataRevocationRequest', contact=subject.contact)
